# Lawnchair Adapter - Cordova File System

## Objective:
The aim of this project is to create a lawnchair adapter script (cordova-fs.js).
This provides a PhoneGap (Cordova) application functionality to store data to the file system of a device.

### Additional goals:

- learn how to implement an automated build process (linting, testing, minification etc.)
- get working experience of TDD / agile development processes so as to find my own process that will work for future projects


## Development
Test Driven Development (TDD) will be used within a basic Agile development process to provide a production level result.  This means that we will have an automated build process and use continuous integration to maintain the project.

## Ackknowledgements

There are numerous resources provided by other people that have made this project possible.

James Shore
Agile Development, TDD and Continuous Integration concepts that have inspired this project to be started.
http://www.letscodejavascript.com/

