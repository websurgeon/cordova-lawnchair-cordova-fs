# debug-ios testapp

This is a basic PhoneGap app for ios generated using the create script provided in PhoneGap download.

## Steps folowed when creating the testapp

- downloaded phonegap-2.4.0zip from http://phonegap.com/download/ and extracted to vendor/cordova folder

- ran ios create script
    
    $ cd vendor/cordova/phonegap-2.4.0/lib/ios/bin
    $ ./create <path-to-this-project>/debug-ios/testapp com.peter-barclay.testapp testapp

- confirmed that generated Xcode project builds ok

- copied contents of www folder to src folder
    - excluding cordova-2.4.0.js
    - excluding spec folder & spec.html as we don't need these Jasmine test files

- created symlinks of src/www content in testapp/www

ln -s <path-to-this-project>/src/www/css
ln -s <path-to-this-project>/src/www/img
ln -s <path-to-this-project>/src/www/index.html
ln -s <path-to-this-project>/src/www/js
ln -s <path-to-this-project>/src/www/res

IMPORTANT:
In the past I have found that using syminks in Xcode project like this works OK when built and run on simulator but does not work properly when run on real device.
A workaround for this is to add a build phase to run a script that copies the www contents.
This should be added before the 'touch www folder' phase.

Script:
cp -r $PROJECT_DIR/../../src/www/* $PROJECT_DIR/www

NB: the Bundle idenitifier com.your_domain_here.testapp should be changed to match the developer certificate used!

Changing the bundle identifier can cause an error when running the app on a device:

    Could not launch '<long-path-to-derived-data-app-file>'. No file or directory found.

This can be resolved by:
- Disconnecting the devide
- Deleting the app from the device
- Quitting Xcode (actually quit, don't just close the window)
- Delete the 'Derived Data'. The location of this can be found by viewing Organiser > Projects
- Re-launch the project in Xcode
- Clean the project
- Plug the device back in
- Run the app on the device

