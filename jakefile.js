/*global task, desc, jake, fail, complete */
/*jslint node: true */

"use strict";
var outputTitle;
var colors = require("./colors.js");

task("default", ["lint"], function () {
    console.log('');
    console.log(colors.white + '==========' + colors.reset);
    console.log('ALL IS OK!');
    console.log(colors.white + '==========' + colors.reset);
});

desc("lint everything");
task("lint", [], function () {
    var lint, files, options, globals, pass;
    lint = require("./build/lint/lint_runner.js");
    outputTitle('Linting Started ...');

    files = new jake.FileList();
    files.include("**/*.js");
    files.exclude(["node_modules", "vendor", "testacular.conf.js", "debug-ios", "src/www/cordova-2.4.0.js"]);

    options = {
        browser: true,
        devel: true,
        indent: 4,
        bitwise: true,
        curly: false,
        eqeqeq: true,
        forin: true,
        immed: true,
        latedef: false,
        newcap: true,
        noarg: true,
        noempty: true,
        nonew: true,
        regexp: true,
        undef: true,
        strict: true,
        trailing: true
    };
    globals = {};

    pass = lint.validateFileList(files.toArray(), options, globals);
    if (pass) {
        outputTitle('Linting Completed OK');
    } else {
        fail(colors.red + 'Lint errors found!' + colors.reset);
    }
});

function outputTitle(title, failed) {
    var color = failed ? colors.red : colors.green;
    console.log('');
    console.log(color + title + colors.reset);
    console.log('');
}