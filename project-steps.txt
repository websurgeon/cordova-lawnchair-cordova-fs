
Estimating time for each engineering task (and sub-tasks) for an MMF allows a minimum development time to be calculated for use in an overall project plan.


Keys:
- item is still to be done
* current item being worked on
> item postponed
+ item is complete


Minimum Marketable Features:

- get method

    User Stories:

    - skeleton file (cordova-fs.js) available 

        Engineering Tasks:

        > automated build
            + install Jake
            + research Jake basics
            + create JakeFile
            > research running xcode build via Jake 
                *** I confirmed that xcode build can be run from command line ***
            - run tests from jake file

        > continuous integration
            > use 'master' branch as integration branch
            + use 'dev' branch for development
            + use bitbucket.org as remote repo
            > clone and build on Windows machine
                *** This is currently not a priority as our target is mobile devices and there are no other developers that need to build this project ***
    
        + create template adapter from existing script (e.g. indexed-db.js)

    + cordova app provide device file system access

        Engineering Tasks:

        + download Cordova source

        + build and run demo app on device

        + create basic www single page app

        + build app for iOS platform

        + get tests accessing cordova API 
            NB: turned out that cordova does not work inside iframe (used by testacular)

    * no-op 'get' method

        Engineering Tasks:

        - spike response of failed 'get' (i.e. no stored data for key)

        - mimic failed 'get' in cordova-fs.js

    - fully working 'get' method

        Engineering Tasks:

        - spike storing text file to file system

        - use spike to store test file for retrieval by 'get' method

        - spike get method to retrieve test file

        - implement 'get' method without error handling

        - handle 'File Not Found' error

        - handle 'JSON parse' failure

        - handle any other errors


