/*global device: true, DirectoryEntry: true, DirectoryReader: true, File: true, FileEntry: true, FileError: true, FileReader: true, FileSystem: true, FileTransfer: true, FileTransferError: true, FileUploadOptions: true, FileUploadResult: true, FileWriter: true, Flags: true, LocalFileSystem: true, Metadata: true, Media: true, openDatabase: true, localStorage: true */
/*jslint indent: 4, browser: true, devel: true */
(function () {
    "use strict";
    var TESTACULAR_SERVER_PROTOCOL = 'http',
     // TESTACULAR_DOMAIN = '10.255.0.87',
        TESTACULAR_SERVER = '192.168.0.12',
        TESTACULAR_SERVER_PORT = 8080,
        TESTACULAR_URL = TESTACULAR_SERVER_PROTOCOL + '://' + TESTACULAR_SERVER + ':' + TESTACULAR_SERVER_PORT,
        testacularFrame = document.getElementById('iframe-testacular');

    function cordovaTestHandler(ev) {
        var msgObj, message;
        if (ev.origin === TESTACULAR_URL) {
            msgObj = JSON.parse(ev.data);
            console.log(msgObj.msgId + ': ' + msgObj.testName);
            if (msgObj.testName === 'getDevice') {
                message = JSON.stringify({
                    testName: msgObj.testName,
                    msgId: msgObj.msgId,
                    data: device
                });
                // console.log('responding to [' + msgObj.testName + ']:');
                // console.log(message);
                testacularFrame.contentWindow.postMessage(message, '*');
            } else {
                console.log(ev.data);
            }
        }// else {
        //     alert(ev.origin + ': ' + ev.data);
        // }
    }

    function listendForCordovaTests() {
        window.addEventListener('message', cordovaTestHandler, false);
    }

    function testGetDevice() {

    }

    function onDeviceReady() {
        listendForCordovaTests();

        testacularFrame.src = TESTACULAR_URL;
    }

    // detect when cordova is loaded and device is ready
    document.addEventListener('deviceready', onDeviceReady, false);

}());
