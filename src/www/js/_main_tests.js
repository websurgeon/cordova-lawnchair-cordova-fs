/*global dump: true, beforeEach: true, afterEach: true, describe: true, it: true, expect: true */
/*jslint indent: 4, browser: true */


(function () {
    "use strict";

    function callCordovaTest(options, testName, data, callback) {
        var message, msgId, timeout;
        options = options || {};
        timeout = options.timeout || 5000;
        msgId = testName + '_' + (new Date()).getTime();
        function messageHandler(ev) {
            var response;
            if (ev.origin === 'file://') {
                response = JSON.parse(ev.data);
                if (response.msgId === msgId) {
                    if (response.err) {
                        callback(response.err);
                    } else {
                        callback(null, response.data);
                    }
                }
            }
            window.parent.removeEventListener('message', messageHandler);
        }
        window.parent.addEventListener('message', messageHandler);
        message = JSON.stringify({
            testName: testName,
            data: data,
            msgId: msgId
        });
        window.top.postMessage(message, '*');
    }

    // beforeEach(function () {

    // });

    // afterEach(function () {

    // });


    describe("Device Support", function () {
        it("should be able to get device info", function (done) {
            this.timeout(2000);
            callCordovaTest({ timeout: 2000 }, 'getDevice', { abc: 123 }, function (err, data) {
                // TODO: work out best way to fail test
                if (err) {
                    dump(JSON.stringify(err));
                    expect(false).ok();
                } else {
                    dump('supports platform: ' + data.platform);
                    expect(true).ok();
                }
                done();
            });
        });

    });

}());