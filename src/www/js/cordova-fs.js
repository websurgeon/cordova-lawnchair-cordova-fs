/**

This adapter has been created to match the API described @ http://brian.io/lawnchair/adapters/

*/

/*globals Lawnchair: true */


Lawnchair.adapter('cordova-fs', (function () {
    "use strict";

    return {

        // adapter name as a string
        adapter: "cordova-fs",

        // boolean; true if the adapter is valid for the current environment
        valid: function () {
            return true;
        },

        // ctor call and callback. 'name' is the most common option
        init: function (options, callback) {

        },

        // returns all the keys in the store
        keys: function (callback) {
            return this;
        },

        // save an object
        save: function (obj, callback) {
            return this;
        },

        // batch save array of objs
        batch: function (array, callback) {
            return this;
        },

        // retrieve obj (or array of objs) and apply callback to each
        get: function (keyOrArray, callback) {
            return this;
        },

        // check if an obj exists in the collection
        exists: function (key, callback) {
            return this;
        },

        // returns all the objs to the callback as an array
        all: function (callback) {
            return this;
        },

        // remove a doc or collection of em
        remove: function (keyOrArray, callback) {
            return this;
        },

        // destroy everything
        nuke: function (callback) {
            return this;
        }

    };

}()));