/*global device: true */
/*jslint indent: 4, browser: true */
(function () {
    "use strict";

    function onDeviceReady() {
        document.getElementById('statusMsg').textContent = 'Device is ready';
    }

    // detect when cordova is loaded and device is ready
    document.addEventListener('deviceready', onDeviceReady, false);

}());
